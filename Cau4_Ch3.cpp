#include <iostream>
#include <string.h>
using namespace std;

void dao_chuoi(char *x, int start, int end)
{
    char ch;
    if (start >= end)
       return;
    ch = *(x+start);
    *(x+start) = *(x+end);
    *(x+end) = ch;
    dao_chuoi(x, ++start, --end);
}
int main()
{
  char string_array[150];
    cout<<"Nhap chuoi: ";
    cin>>string_array;
    dao_chuoi(string_array, 0, strlen(string_array)-1);
    cout<<"Chuoi sau khi dao nguoc: "<<string_array;
    return 0;
}

